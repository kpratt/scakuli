package ca.dualitystudios.sikuli.scala.api

import org.specs2.mutable.Specification

object Test extends Specification {
  "x" should {
    "y" in {
      val chrome = Resource("sikuli-5474707763670498076.png")
      for(icon <- Sikuli.find(chrome)) {
        icon.click()
      }
      success
    }
  }
}
