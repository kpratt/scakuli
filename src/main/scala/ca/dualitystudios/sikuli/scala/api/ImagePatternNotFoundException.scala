package ca.dualitystudios.sikuli.scala.api


import org.sikuli.api.{ScreenRegion, ImageTarget, DesktopScreenRegion}
import javax.imageio.ImageIO
import scala.collection.JavaConverters._
import org.sikuli.api.robot.desktop.{DesktopKeyboard, DesktopMouse}
import com.sun.media.renderer.video.AWTRenderer
import scala.util.Try
import java.awt.image.BufferedImage

class ImagePatternNotFoundException(screenCapture: BufferedImage, innerException: Exception) extends Exception(innerException) {

}