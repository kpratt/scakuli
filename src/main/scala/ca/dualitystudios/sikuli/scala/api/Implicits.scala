package ca.dualitystudios.sikuli.scala.api

import scala.collection.JavaConverters._
import scala.util.Try

object Implicits {
  implicit def optionToReduceAble[T](op: Option[T]): ReduceAble[T] = new ReduceAble(op)
  implicit def tryToReduceAble[T](op: Try[T]): ReduceAble[T] = new ReduceAble(op.toOption)
}

class ReduceAble[T](op: Option[T]) {
  def or(t: => Option[T]): Option[T] = if(op.isDefined) op else t
  def or(t: Try[T]): Option[T] = if(op.isDefined) op else t.toOption

}