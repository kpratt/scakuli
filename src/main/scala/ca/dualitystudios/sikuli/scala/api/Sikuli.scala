package ca.dualitystudios.sikuli.scala.api


import org.sikuli.api.{ScreenRegion, ImageTarget, DesktopScreenRegion}
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import scala.collection.JavaConverters._
import org.sikuli.api.robot.desktop.{DesktopKeyboard, DesktopMouse}
import com.sun.media.renderer.video.AWTRenderer
import scala.util.Try
import org.sikuli.api.visual.{DesktopCanvas, Canvas}

object Sikuli extends Sikuli(new DesktopScreenRegion())

class Sikuli(val region: ScreenRegion) {

  def find(resource: Resource): Try[Target] = {
    Try {
      try {
        findAll(resource).head
      } catch {
        case e: NoSuchElementException =>
          throw new ImagePatternNotFoundException(region.capture().asInstanceOf[BufferedImage], e)
      }
    }
  }

  def findAll(resource: Resource): Seq[Target] = {
    val image = ImageIO.read(resource.asStream)
    val target = new ImageTarget(image)
    region.findAll(target).asScala.sortBy( 0 - _.getScore() ).map(new Target(_))
  }

  def exits(resource: Resource): Boolean = {
    findAll(resource).headOption.isDefined
  }

  val keyboard = new DesktopKeyboard
  def sendKeys(keystrokes: String): Unit = {
    keyboard.`type`(keystrokes)
  }
  def sendKey(key: Key): Unit = {
    keyboard.keyDown(key.id)
    keyboard.keyUp(key.id)
  }

  def targetLocation(xoffset: Int, yoffset: Int): Target = {
    val x = if(xoffset >= 0) xoffset else xoffset + region.getBounds.getWidth.toInt
    val y = if(yoffset >= 0) yoffset else yoffset + region.getBounds.getHeight.toInt
    Target(region.getRelativeScreenRegion(x, y, 0, 0))
  }

  def forRegion(xoffset: Int, yoffset: Int, width: Int, height: Int): Sikuli =
    new Sikuli(region.getRelativeScreenRegion(xoffset, yoffset, width, height))
}

object Target {
  def apply(region: ScreenRegion) = new Target(region)
}

class Target(val region: ScreenRegion, xOffset: Int = 0, yOffset: Int = 0) {

  val mouse = new DesktopMouse();

  def outline(): Target = {
    val c = new DesktopCanvas()
    c.addBox(region)
    c.display(3)
    this
  }

  def click(): Unit = {
    mouse.click(region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
  }

  def doubleClick(): Unit = {
    mouse.doubleClick(region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
  }

  def rightClick(): Unit = {
    mouse.rightClick(region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
  }

  def hover(): Unit = {
    throw new NotImplementedError("'hover' waiting on next version of sikuli (1.0.3)")
    //mouse.hover(region.getRelativeScreenLocation(xOffset, yOffset))
  }

  def dragTo(other: Target): Unit = {
    mouse.drag(region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
    mouse.drop(other.region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
  }

  def dragTo(other: ScreenRegion): Unit = {
    mouse.drag(region.getCenter.getRelativeScreenLocation(xOffset, yOffset))
    mouse.drop(other.getCenter.getRelativeScreenLocation(xOffset, yOffset))
  }

}

case class Resource(uri: String){
  def asStream = this.getClass.getClassLoader.getResourceAsStream(uri)
}

case class Key(val id: Int)
object Keys {
  val ENTER = Key(java.awt.event.KeyEvent.VK_ENTER)
  val ESCAPE = Key(java.awt.event.KeyEvent.VK_ESCAPE)
}