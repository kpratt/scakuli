import sbt._
import Keys._

object Build extends Build {

  override lazy val settings = super.settings ++ Seq(
    scalaVersion := "2.10.1",

    //resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
    resolvers += "JavaCPP" at "http://maven2.javacpp.googlecode.com/git/",
    resolvers += "uni-due.de" at "http://mvn.is.inf.uni-due.de:8081/nexus/content/repositories/atunes-dependencies/",


    libraryDependencies ++= Seq(
        "commons-io"               %  "commons-io"              % "2.3"

//      , "com.googlecode.javacpp"   %  "javacpp"                 % "0.1"
//      , "commons-cli"              % "commons-cli"              % "1.2"
//      , "com.melloware"            % "jintellitype"             % "1.3.7"
//      , "jxgrabkey"                % "jxgrabkey"                % "1.0"
//      , "org.sikuli"               % "SikuliX-Basics"           % "1.0.1"
//      , "org.sikuli"               % "sikuli-core"              % "1.1.2"

//      , "org.sikuli"               %  "sikuli-api"              % "1.0.1"

      , "org.specs2"              %%      "specs2"              % "2.1.1"  % "test"
      , "junit"                    %      "junit"               % "4.11"   % "test"

    ))

  lazy val root =
    Project(id = "Scakuli",
      base = file("."),
      settings = Project.defaultSettings)
}
